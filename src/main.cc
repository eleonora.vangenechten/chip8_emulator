#include <chrono>
#include <iostream>
#include "SDL.h"
#include "chip8.h"

constexpr int SCALE = 30;
const char *TESTROMLOC =
    "/home/eleonora/Programming/chip8_emulator/test_roms/pong.rom";

void logSDLError(const std::string &error_msg) {
  std::cout << error_msg << " error: " << SDL_GetError() << "\n";
}

void SDL_Cleanup(SDL_Renderer *ren, SDL_Window *win) {
  SDL_DestroyRenderer(ren);
  SDL_DestroyWindow(win);
  SDL_Quit();
}

int main(int argc, char **argv) {
  if (SDL_Init(SDL_INIT_VIDEO) != 0) {
    logSDLError("SDL_INIT_VIDEO");
    SDL_Cleanup(nullptr, nullptr);
    return 1;
  }
  if (SDL_Init(SDL_INIT_AUDIO) != 0) {
    logSDLError("SDL_INIT_AUDIO");
    SDL_Cleanup(nullptr, nullptr);
    return 1;
  }
  // Create window
  // Scale the 64x32 window.
  SDL_Window *win = SDL_CreateWindow("Hello World!", 500, 500, 64 * SCALE,
                                     32 * SCALE, SDL_WINDOW_SHOWN);

  if (win == nullptr) {
    logSDLError("SDL_WINDOW");
    SDL_Cleanup(nullptr, win);
    return 1;
  }

  SDL_Renderer *ren = SDL_CreateRenderer(
      /* associated window= */ win,
      /* rendering driver index=  */ -1,
      /* flags= */ SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

  if (ren == nullptr) {
    logSDLError("SDL_CreateRenderer");
    SDL_Cleanup(nullptr, win);
    return 1;
  }


  // Initialize emulator.
  chip8_emulator::chip8::chip8 emulator;
  emulator.initialize();

  // TODO: Change game path to be loaded differently.
  FILE *fp = std::fopen(/*game location=*/TESTROMLOC, "rb");

  if (!fp) {
    std::perror("File opening failed");
    return EXIT_FAILURE;
  }

  std::vector<int> char_buffer;
  int c;
  while ((c = std::fgetc(fp)) != EOF) {  // standard C I/O file reading loop
    char_buffer.push_back(c);
  }

  if (std::ferror(fp))
    std::puts("I/O error when reading");
  else if (std::feof(fp))
    std::puts("End of file reached successfully");
  std::fclose(fp);

  // Boot is a CHIP-8 program that can be loaded in case nothing is.
  // int boot[] = {
  // 0xA2, 0x5B, 0x60, 0x0B, 0x61, 0x03, 0x62, 0x07, 0xD0, 0x17, 0x70, 0x07,
  // 0xF2, 0x1E, 0xD0, 0x17, 0x70, 0x07, 0xF2, 0x1E, 0xD0, 0x17, 0x70, 0x07,
  // 0xF2, 0x1E, 0xD0, 0x17, 0x70, 0x07, 0xF2, 0x1E, 0xD0, 0x17, 0x70, 0x05,
  // 0xF2, 0x1E, 0xD0, 0x17, 0xF2, 0x1E, 0xA2, 0x5A, 0xC0, 0x3F, 0xC1, 0x1F,
  // 0x62, 0x01, 0x63, 0x01, 0xD0, 0x11, 0x64, 0x02, 0xF4, 0x15, 0xF4, 0x07,
  // 0x34, 0x00, 0x12, 0x3A, 0xD0, 0x11, 0x80, 0x24, 0x81, 0x34, 0xD0, 0x11,
  // 0x41, 0x00, 0x63, 0x01, 0x41, 0x1F, 0x63, 0xFF, 0x40, 0x00, 0x62, 0x01,
  // 0x40, 0x3F, 0x62, 0xFF, 0x12, 0x36, 0x80, 0x78, 0xCC, 0xC0, 0xC0, 0xC0,
  // 0xCC, 0x78, 0xCC, 0xCC, 0xCC, 0xFC, 0xCC, 0xCC, 0xCC, 0xFC, 0x30, 0x30,
  // 0x30, 0x30, 0x30, 0xFC, 0xF8, 0xCC, 0xCC, 0xF8, 0xC0, 0xC0, 0xC0, 0x00,
  // 0x00, 0x00, 0xF0, 0x00, 0x00, 0x00, 0x78, 0xCC, 0xCC, 0x78, 0xCC, 0xCC,
  // 0x78,
  //};

  emulator.loadGame(&char_buffer[0], char_buffer.size());

  // Setup graphics
  SDL_RenderClear(ren);
  SDL_RenderPresent(ren);

  // Main game loop.
  SDL_Event e;
  bool quit = false;
  auto begin = std::chrono::system_clock::now();
  while (!quit) {
    // Only emulate a cycle if 1/60 of a second passed.
    typedef std::chrono::duration<float> float_seconds;
    if (std::chrono::duration_cast<float_seconds>(
            std::chrono::system_clock::now() - begin)
            .count() >= (1 / 60)) {
      begin = std::chrono::system_clock::now();
      emulator.emulateCycle();

    // Render if the drawFlag is set.
    if (emulator.drawFlag) {
      SDL_SetRenderDrawColor(ren, 0, 0, 0, SDL_ALPHA_OPAQUE);
      SDL_RenderClear(ren);

      // Draw a scale x scale rectangle for every pixel
      // that is activated in the chip8.
      SDL_SetRenderDrawColor(ren, 255, 255, 255, SDL_ALPHA_OPAQUE);
      std::vector<SDL_Rect> rects;
      rects.clear();
      int num_rects = 0;

      for (int i = 0; i < 32; i++) {
        for (int j = 0; j < 64; j++) {
          if (emulator.gfx[i * 64 + j]) {
            num_rects++;
            SDL_Rect rect;
            rect.x = j * SCALE;
            rect.y = i * SCALE;
            rect.w = SCALE;
            rect.h = SCALE;
            rects.push_back(rect);
          }
        }
      }

      if (rects.size() > 0) {
        SDL_RenderDrawRects(ren, &rects[0], num_rects);
        SDL_RenderFillRects(ren, &rects[0], num_rects);
      }
      SDL_RenderPresent(ren);
      emulator.drawFlag = false;
      rects.clear();
    }
    }

    // input polling and set keyspress state in the chip8.
    // The key mapping is as follows
    // 1 = 1, 2 = 2, 3=3, c = 4, 4 =Q, 5 = w, 6 = e, d = r,
    // 7=a,8=s,9=d,e=f,a=z,0=x,b=c,f=v
    bool event_occurred = SDL_PollEvent(&e);

    if (event_occurred) {
      // Window is closed.
      if (e.type == SDL_QUIT) {
        quit = true;
      }
      if (e.type == SDL_KEYDOWN) {
        switch (e.key.keysym.sym) {
          case SDLK_1:
            emulator.key[0] = true;
            break;
          case SDLK_2:
            emulator.key[1] = true;
            break;
          case SDLK_3:
            emulator.key[2] = true;
            break;
          case SDLK_q:
            emulator.key[3] = true;
            break;
          case SDLK_w:
            emulator.key[4] = true;
            break;
          case SDLK_e:
            emulator.key[5] = true;
            break;
          case SDLK_a:
            emulator.key[6] = true;
            break;
          case SDLK_s:
            emulator.key[7] = true;
            break;
          case SDLK_d:
            emulator.key[8] = true;
            break;
          case SDLK_z:
            emulator.key[9] = true;
            break;
          case SDLK_x:
            emulator.key[10] = true;
            break;
          case SDLK_c:
            emulator.key[11] = true;
            break;
          case SDLK_4:
            emulator.key[12] = true;
            break;
          case SDLK_r:
            emulator.key[13] = true;
            break;
          case SDLK_f:
            emulator.key[14] = true;
            break;
          case SDLK_v:
            emulator.key[15] = true;
            break;
        }
      }
      if (e.type == SDL_KEYUP) {
        switch (e.key.keysym.sym) {
          case SDLK_1:
            emulator.key[0] = false;
            break;
          case SDLK_2:
            emulator.key[1] = false;
            break;
          case SDLK_3:
            emulator.key[2] = false;
            break;
          case SDLK_q:
            emulator.key[3] = false;
            break;
          case SDLK_w:
            emulator.key[4] = false;
            break;
          case SDLK_e:
            emulator.key[5] = false;
            break;
          case SDLK_a:
            emulator.key[6] = false;
            break;
          case SDLK_s:
            emulator.key[7] = false;
            break;
          case SDLK_d:
            emulator.key[8] = false;
            break;
          case SDLK_z:
            emulator.key[9] = false;
            break;
          case SDLK_x:
            emulator.key[10] = false;
            break;
          case SDLK_c:
            emulator.key[11] = false;
            break;
          case SDLK_4:
            emulator.key[12] = false;
            break;
          case SDLK_r:
            emulator.key[13] = false;
            break;
          case SDLK_f:
            emulator.key[14] = false;
            break;
          case SDLK_v:
            emulator.key[15] = false;
            break;
        }
      }
    }
  }

  SDL_Cleanup(nullptr, win);
  return 0;
}
