#ifndef CHIP_8_H
#define CHIP_8_H
#include <vector>
#include "SDL.h"

namespace chip8_emulator {
namespace chip8 {

class chip8 {
 public:
  unsigned char CHIP8_FONTSET[80] = {
      0xF0, 0x90, 0x90, 0x90, 0xF0,  // 0
      0x20, 0x60, 0x20, 0x20, 0x70,  // 1
      0xF0, 0x10, 0xF0, 0x80, 0xF0,  // 2
      0xF0, 0x10, 0xF0, 0x10, 0xF0,  // 3
      0x90, 0x90, 0xF0, 0x10, 0x10,  // 4
      0xF0, 0x80, 0xF0, 0x10, 0xF0,  // 5
      0xF0, 0x80, 0xF0, 0x90, 0xF0,  // 6
      0xF0, 0x10, 0x20, 0x40, 0x40,  // 7
      0xF0, 0x90, 0xF0, 0x90, 0xF0,  // 8
      0xF0, 0x90, 0xF0, 0x10, 0xF0,  // 9
      0xF0, 0x90, 0xF0, 0x90, 0x90,  // A
      0xE0, 0x90, 0xE0, 0x90, 0xE0,  // B
      0xF0, 0x80, 0x80, 0x80, 0xF0,  // C
      0xE0, 0x90, 0x90, 0x90, 0xE0,  // D
      0xF0, 0x80, 0xF0, 0x80, 0xF0,  // E
      0xF0, 0x80, 0xF0, 0x80, 0x80   // F
  };

  // The chip8 has 4Kb of memory
  // with the following system memory map:
  // 0x000-0x1FF - The Chip8 interpreter (contains font set)
  // 0x050-0x0A0 - Used for the built in 4x5 pixel font
  // 0x200-0xFFF - Program ROM and work RAM
  unsigned char memory[4096];

  // CPU registers.
  // 15 8-bit general purpose registers V0, ..., VE
  // The 16th register is used for the 'carry flag' VF, this carry flag can also
  // be used for collision detection, because it is set to 1 if screen pixels
  // are flipped from set to unset when a sprite is drawn.
  unsigned char V[16];

  // The address register
  // used with opcodes that involve memory operations.
  unsigned short I;

  // Program counter
  unsigned short pc;

  // The two chip8 timers.
  // They should count at 60 Hz and count down to 0 when set above zero.
  unsigned char delay_timer;
  unsigned char sound_timer;

  // Soundwave
  SDL_AudioSpec wavSpec;
  Uint32 wavLength;
  Uint8* wavBuffer;

  // Audio device
  SDL_AudioDeviceID deviceId;

  // A 16 level stack.
  // The stack is used to remember the current location before a jump.
  // The program counter gets stored on the stack before proceeding further.
  // The stack pointer gets used to remember which level of the stack is used.
  unsigned short stack[16];
  unsigned short sp;

  unsigned short fetchOpcode();
  void executeOpcode(unsigned short opcode);

  void clearDisplay();

 // Graphics system.
 // The screen has a total of 2048 pixels.
 // 64 x 32
 unsigned char gfx[64 * 32];

 // The HEX based keypad (0x0 - 0xF) of the Chip8, where key[0] is the 0x0 and
 // key[15] is 0xF True if the key is pressed, false otherwise.
 unsigned char key[16];

 bool drawFlag;
 void initialize();
 void emulateCycle();
 void loadGame(int* file_buffer, int bufferSize);
};

} // namespace chip8
} // namespace chip8_emulator
#endif
