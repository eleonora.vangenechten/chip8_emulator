#include "chip8.h"
#include <unistd.h>
#include <iostream>
#include "SDL.h"

namespace chip8_emulator {
namespace chip8 {

// Initialize registers and memory.
void chip8::initialize() {

  pc = 0x200; // The system expects the application to be loaded at 0x200
  I = 0;      // Reset address register
  sp = 0;     // Reset the stack pointer
  drawFlag = true;
  clearDisplay();

  // Clear keyboard
  for (int i = 0; i < 15; i++) {
    key[i] = false;
  }

  // Clear the stack and registers
  for (int i = 0; i < 16; i++) {
    stack[i] = 0x0;
    V[i] = 0x0;
  }

  // Clear the memory
  // from byte 512 to 4096
  for (int i = 0x200; i < 0xFFF; i++) {
    memory[i] = 0x0;
  }

  // Load fontset
  for (int i = 0; i < 0x50; i++) {
    memory[i] = CHIP8_FONTSET[i];
  }

  // Reset timers
  delay_timer = 0;
  sound_timer = 0;

  // seed for random generator.
  std::srand(time(nullptr));

  // load WAV file
  SDL_LoadWAV("/home/eleonora/Programming/chip8_emulator/resources/beep-07.wav",
              &wavSpec, &wavBuffer, &wavLength);

  // Init handle to audiodevice.
  deviceId = SDL_OpenAudioDevice(NULL, 0, &wavSpec, NULL, 0);
}

void chip8::emulateCycle() {
  // Fetch Opcode, from memory at the program counter location.
  // Because one opcode is 2 bytes, we need to fetch 2 successive bytes to get
  // the actual opcode.
  unsigned short opcode = fetchOpcode();
  std::cout << "opcode 0x" << std::hex << opcode << "\n";

  // Decode and execute Opcode
  executeOpcode(opcode);

  // Update timers
  if (delay_timer > 0) {
    delay_timer--;
  }
  if (sound_timer > 0) {
    sound_timer--;
    // play audio
    SDL_QueueAudio(deviceId, wavBuffer, wavLength);
    SDL_PauseAudioDevice(deviceId, 0);

  } else {
    SDL_PauseAudioDevice(deviceId, 1);
  }
}

unsigned short chip8::fetchOpcode() {
  // Read the first 8 bits, shift left and read the next 8 bits.
  // Uses the bitwise or.
  return memory[pc] << 8 | memory[pc + 1];
}

void chip8::executeOpcode(unsigned short opcode) {
  // std::cout << "program counter:" << std::dec << pc << "\n ";

  // Start by reading the first 4 bits of the opcode.
  switch (opcode & 0xF000) {
    case 0x0000: {
      // Because there are multiple opcodes where the first 4 bits are 0x000,
      // it is necessary to check the last 4 bits of their opcode
      switch (opcode & 0x000F) {
        case 0x0000: {
          // 0x00E0
          for (int i = 0; i < 2048; ++i) {
            gfx[i] = 0;
          }
          drawFlag = true;
          pc += 2;
          break;

          // clearDisplay();
          // drawFlag = true;
          // pc += 2;
          // break;
        }
        case 0x000E: {
          // 0x00EE
          // returns from subroutine
          if (sp > 0) {
            sp--;
          }
          pc = stack[sp];
          pc += 2;
          break;
        }
        default: {
          std::cout << "unknown opcode"
                    << "\n";
          break;
        }
      }
      break;
    }  // end case 0x0000

    case 0x1000: {
      // Jump to address NNN.
      pc = opcode & 0x0FFF;
      break;
    }

    case 0x2000: {
      // Call subroutine at NNN.
      if (sp <= 15) {
        stack[sp++] = pc;
        pc = opcode & 0x0FFF;

      } else {
        std::cout << "stack overflow"
                  << "\n";
      }
      break;
    }

    case 0x3000: {
      // Skip next instruction if Vx == NN.
      unsigned short x = (opcode & 0x0F00) >> 8;
      unsigned short num = opcode & 0x00FF;
      if (V[x] == num) {
        // Skip the next instruction.
        pc += 2;
      }
      pc += 2;
      break;
    }

    case 0x4000: {
      // Skip next instruction if Vx != NN.
      unsigned short x = (opcode & 0x0F00) >> 8;
      unsigned short num = opcode & 0x00FF;
      if (V[x] != num) {
        // Skip the next instruction.
        pc += 2;
      }
      pc += 2;
      break;
    }

    case 0x5000: {
      unsigned short x = (opcode & 0x0F00) >> 8;
      unsigned short y = (opcode & 0x00F0) >> 4;
      if (V[x] == V[y]) {
        // Skip the next instruction.
        pc += 2;
      }
      pc += 2;
      break;
    }

    case 0x6000: {
      unsigned short x = (opcode & 0x0F00) >> 8;
      unsigned short nn = opcode & 0x00FF;
      V[x] = nn;
      pc += 2;
      break;
    }

    case 0x7000: {
      unsigned short x = (opcode & 0x0F00) >> 8;
      unsigned short nn = opcode & 0x00FF;
      V[x] = (V[x] + nn) & 0xff;
      pc += 2;
      break;
    }

    case 0x8000: {
      unsigned short x = (opcode & 0x0F00) >> 8;
      unsigned short y = (opcode & 0x00F0) >> 4;

      switch (opcode & 0x000F) {
        case 0x0000: {
          V[x] = V[y];
          pc += 2;
          break;
        }
        case 0x0001: {
          // Opcode 8XY1
          // Perform a bitwise or.
          V[x] |= V[y];
          pc += 2;
          break;
        }
        case 0x0002: {
          // Opcode 8XY2
          // Perform a bitwise and.
          V[x] &= V[y];
          pc += 2;
          break;
        }
        case 0x0003: {
          // Opcode 8XY3
          // a bitwise XOR.
          V[x] ^= V[y];
          pc += 2;
          break;
        }
        case 0x0004: {
          // Opcode 8XY4
          // add V[Y] to V[X]

          // V[f] is set to 0 if there is a carry, and to 1 otherwise.
          int res = V[x] + V[y];
          if (res > 0x00FF) {
            V[15] = 1;
          } else {
            V[15] = 0;
          }
          V[x] = res;
          pc += 2;
          break;
        }
        case 0x0005: {
          // Opcode 8XY5
          // Substract V[y] from V[x].

          // V[f] is set to 0 if there is a borrow, and to 1 otherwise.
          if (V[x] >= V[y]) {
            // no borrow.
            V[15] = 1;
          } else {
            // borrow.
            V[15] = 0;
          }
          V[x] -= V[y];
          pc += 2;
          break;
        }
        case 0x0006: {
          // Opcode 8XY6
          // Bitwise operation which stores the least significant bit in V[f]
          V[15] = V[y] & 0x1;
          V[x] = V[y] >> 1;
          pc += 2;
          break;
        }
        case 0x0007: {
          // Opcode 8XY7
          if (V[y] >= V[x]) {
            // no borrow.
            V[15] = 1;
          } else {
            // borrow.
            V[15] = 0;
          }
          V[x] = (V[y] - V[x]);
          pc += 2;
          break;
        }
        case 0x000E: {
          // Check if the most significant digit is set.
          V[15] = V[y] >> 7;
          V[x] = V[y] << 1;
          pc += 2;
          break;
        }
        default:
          std::cout << "unknown opcode"
                    << "\n";
          break;
      }
      break;
    }

    case 0x9000: {
      int x = (opcode & 0x0F00) >> 8;
      int y = (opcode & 0x00F0) >> 4;
      if (V[x] != V[y]) {
        pc += 2;
      }
      pc += 2;
      break;
    }

    case 0xA000: {
      int nnn = opcode & 0x0FFF;
      I = nnn;
      pc += 2;
      break;
    }

    case 0xB000: {
      int nnn = opcode & 0x0FFF;
      pc = V[0] + nnn;
      break;
    }

    case 0xC000: {
      int x = (opcode & 0x0F00) >> 8;
      int kk = opcode & 0x00FF;

      int rnd = (std::rand() % (0xFF + 1));
      V[x] = kk & rnd;
      pc += 2;
      break;
    }

    case 0xD000: {
      int x = (opcode & 0x0F00) >> 8;
      int y = (opcode & 0x00F0) >> 4;
      int n = opcode & 0x000F;

      // Draws a sprite at coordinate (VX, VY) that has a width of 8 pixels
      // and a height of N pixels. Each row of 8 pixels is read as bit-coded
      // starting from memory location I.
      // VF is set to 1 if any screen pixels are flipped from set to unset
      // when / the sprite is drawn,
      // and to 0 if that doesn’t happen.
      unsigned char Vx = V[x];
      unsigned char Vy = V[y];
      V[0xF] = 0;
      for (int i = 0; i < n; i++) {
        int pixel_val = memory[I + i];
        for (int j = 0; j < 8; j++) {
          if ((pixel_val & (0x80 >> j)) != 0) {
            int coord = (64 * (Vy + i) + Vx + j) % 0x800;
            if (gfx[coord]) {
              V[0xF] = 1;
            }
            // xor with current pixel value.
            gfx[coord] ^= 1;
          }
        }
      }
      drawFlag = true;
      pc += 2;
      break;
    }
      case 0xE000: {
        int x = (opcode & 0x0F00) >> 8;

        switch (opcode & 0x00FF) {
          case 0x009E: {
            if (key[V[x]] > 0) {
              // Skip next instruction.
              pc += 2;
            }
            pc += 2;
            break;
          }
          case 0x00A1: {
            if (key[V[x]] == 0) {
              // skip next instruction.
              pc += 2;
            }
            pc += 2;
            break;
          }
        }
        break;
    }
    case 0xF000: {
      int x = (opcode & 0x0F00) >> 8;
      switch (opcode & 0x00FF) {
        case 0x0007: {
          // set Vx to the delay timer value;
          V[x] = delay_timer;
          pc += 2;
          break;
        }
        case 0x000A: {
          // Blocking operation. Wait for keypress.
          // Do not increment program counter until keypress detected.
          // std::cout << "wait for key"
          //<< "\n";
          for (int i = 0; i <= 0xF; i++) {
            if (key[i]) {
              // store the value of the pressed key in V[x].
              V[x] = i;
              pc += 2;
            }
          }
          break;
        }
        case 0x0015: {
          delay_timer = V[x];
          pc += 2;
          break;
        }
        case 0x0018: {
          sound_timer = V[x];
          pc += 2;
          break;
        }
        case 0x001E: {
          int tmp = I + V[x];
          if (tmp > 0xFFF) {
            V[0xF] = 1;
          } else {
            V[0xF] = 0;
          }
          I = tmp;
          pc += 2;
          break;
        }

        case 0x0029: {
          I = V[x] * 0x5;
          pc += 2;
          break;
        }

        case 0x0033: {
          memory[I] = V[x] / 100;
          memory[I + 1] = (V[x] / 10) % 10;
          memory[I + 2] = (V[x] % 100) % 10;
          pc += 2;
          break;
        }
        case 0x0055: {
          for (int i = 0; i <= x; i++) {
            memory[I + i] = V[i];
          }
          I += x + 1;
          pc += 2;
          break;
        }
        case 0x0065: {
          for (int i = 0; i <= x; i++) {
            V[i] = memory[I + i];
          }

          I += x + 1;
          pc += 2;
          break;
        }
      }
      break;
    }
    default: {
      // Do nothing and skip opcode.
      std::cout << "unknown opcode"
                << "\n";
      pc += 2;
      break;
    }
  }
}

void chip8::loadGame(int* file_buffer, int bufferSize) {
  std::cout << "Game of size " << bufferSize << " loaded"
            << "\n";
  for (int i = 0; i < bufferSize; i++) {
    memory[i + 512] = file_buffer[i];
  };
}

void chip8::clearDisplay() {
  for (int i = 0; i < 2048; i++) {
    gfx[i] = false;
  }
}

} // namespace chip8
} // namespace chip8_emulator

