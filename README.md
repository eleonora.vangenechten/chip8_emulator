# CHIP8_emulator

An emulator that is able to run games written in the chip-8 language.

The build system used is bazel. To run the program execute:
`bazel run //src:main`